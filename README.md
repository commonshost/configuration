# @commonshost/configuration ⚙️

These are the schemas for [@commonshost/server](https://www.npmjs.com/package/@commonshost/server) configuration files. They are shared between various related projects. This package acts as the versioned reference.

## API

### `validate(json)`

Throws the first encountered error if the configuration syntax is invalid, otherwise returns `true`.

```js
const { validate } = require('@commonshost/configuration')

const configuration = {
  hosts: [
    {
      domain: 'commons.host',
      root: 'public'
    }
  ]
}

validate(configuration)
```

### `normaliseHost(host, options)`

Process an individual host instance to return a cleaned up representation.

```js
const { normaliseHost } = require('@commonshost/configuration')

const normalised = await normaliseHost(host, options)
```

Returns a promise which resolves to a host object with various sanity checks applied.

Options:

- `externalManifest`: A boolean (default: `false`) that enables (`true`) or disables (`false`) the resolving of external HTTP/2 server push manifests.

- `configurationFilepath`: The file path, as a string, of the configuration file to which any external manifest should be resolved. If not specified, external manifests will not be resolved. Example: `/etc/commonshost/server.json`

### `findFile(cwd)`

Looks for a configuration file matching `commons((.)host)(.conf(ig)).js(on)` in the given directory.

`cwd` defaults to the current working directory.

Returns the filepath if a configuration file is found.

The configuration file name is any permutation of these pre-, in-, and suffixes.

| Prefix | Infix | Suffix |
|-|-|-|
| commons | .conf | .js |
| commonshost | .config | .json |
| commons.host | *none* |  |

### `loadFile(filepath)`

Loads a configuration file that can be a JSON file or a CommonsJS module.

For JSON, the object is returned.

```json
{
  "hosts": [
    {
      "domain": "example.com"
    }
  ]
}
```

If a CommonJS module exports an object then it is simply returned.

```js
module.exports = {
  hosts: [
    {
      domain: 'example.com'
    }
  ]
}
```

If a CommonJS module exports a function, it is evaluated. Async functions that return a promise are awaited.

```js
module.exports = async function () {
  const response = await fetch('https://example.com/commonshost.json')
  // Response body: {"hosts":[{"domain":"example.com"}]}
  return response.json()
}
```

For each of these configuration file examples, the result is the same.

```js
const configuration = await loadFile(filepath)
// {
//   hosts: [
//     {
//       domain: 'example.com'
//     }
//   ]
// }
```

## JSON Schemas

### Server

The server schema defines the main structure of a configuration file. It contains options that apply to the overall server instance.

```js
const serverSchema = require('@commonshost/configuration/schema/server')
```

### Host

The schema specifically for validating a `host` section of the configuration. It contains options that apply to an individual site by domain.

```js
const hostSchema = require('@commonshost/configuration/schema/host')
```

### Manifest

The HTTP/2 Server Push Manifest schema is available in [@commonshost/manifest](https://www.npmjs.com/package/@commonshost/manifest) and used to validate the `manifest` property of a host.

```js
const { schema: manifestSchema } = require('@commonshost/manifest')
```
