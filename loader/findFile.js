const { join } = require('path')
const pathExists = require('path-exists')

const prefixes = [
  'commonshost',
  'commons',
  'commons.host'
]

const infixes = [
  '',
  '.conf',
  '.config'
]

const suffixes = [
  '.js',
  '.json'
]

async function findFile (cwd = process.cwd()) {
  for (const prefix of prefixes) {
    for (const infix of infixes) {
      for (const suffix of suffixes) {
        const filename = prefix + infix + suffix
        const filepath = join(cwd, filename)
        if (await pathExists(filepath)) {
          return filepath
        }
      }
    }
  }
}

module.exports.findFile = findFile
