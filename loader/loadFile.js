async function loadFile (filepath) {
  let loaded
  try {
    loaded = require(filepath)
  } catch (error) {
    if (error.code === 'MODULE_NOT_FOUND') {
      throw new Error(error.message.split('\n')[0])
    } else {
      throw error
    }
  }
  delete require.cache[filepath]
  return typeof loaded === 'function' ? loaded() : loaded
}

module.exports.loadFile = loadFile
