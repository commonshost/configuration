const { Parser } = require('expr-eval')

const whitelist = ['max_physical_cpu_cores']

module.exports.calculationFormat =
function calculationFormat (pattern) {
  try {
    const variables = Parser.parse(pattern).variables()
    return variables.every((variable) => whitelist.includes(variable))
  } catch (error) {
    return false
  }
}
