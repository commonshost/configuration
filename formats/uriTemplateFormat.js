const UriTemplate = require('uri-templates')

module.exports.uriTemplateFormat =
function uriTemplateFormat (input) {
  try {
    UriTemplate(input)
  } catch (error) {
    return false
  }
  return true
}
