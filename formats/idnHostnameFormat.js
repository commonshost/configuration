const regex = require('idn-allowed-code-points-regex')

module.exports.idnHostnameFormat =
function idnHostnameFormat (input) {
  if (typeof input !== 'string') return false
  if (input.length > 253) return false
  for (const part of input.split('.')) {
    if (part.length > 63) return false
    if (!regex.test(part)) return false
  }
  return true
}
