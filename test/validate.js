const test = require('blue-tape')
const { domainToASCII: toASCII } = require('url')
const { validate } = require('..')

test('Check a valid blank config', async (t) => {
  const given = {}
  t.doesNotThrow(() => validate(given))
})

test('Check an invalid config', async (t) => {
  const given = { foo: 'bar' }
  t.throws(() => validate(given))
})

test('Check a custom type with anyOf', async (t) => {
  const given = {
    workers: {
      count: 'max_physical_cpu_cores / 2'
    }
  }
  t.doesNotThrow(() => validate(given))
})

test('Check README example', async (t) => {
  const given = { hosts: [{ domain: 'commons.host', root: 'public' }] }
  t.doesNotThrow(() => validate(given))
})

test('Check fallback as string', async (t) => {
  const given = { hosts: [{ fallback: '/index.html' }] }
  t.doesNotThrow(() => validate(given))
})

test('Check manifest as file path', async (t) => {
  const filepath = 'filename.json'
  const given = { hosts: [{ manifest: filepath }] }
  t.doesNotThrow(() => validate(given))
})

test('Check manifest as Manifest instance', async (t) => {
  const manifest = []
  const given = { hosts: [{ manifest }] }
  t.doesNotThrow(() => validate(given))
})

test('Check fallback as status code map', async (t) => {
  const given = {
    hosts: [{
      fallback: {
        200: '/200.html',
        404: '/404.html'
      }
    }]
  }
  t.doesNotThrow(() => validate(given))
})

test('Check invalid fallback status code', async (t) => {
  const given = { hosts: [{ fallback: { 666: '/evil.html' } }] }
  t.throws(() => validate(given))
})

test('Check fallback does not have other options', async (t) => {
  const given = {
    hosts: [{
      fallback: {
        foo: 'bar'
      }
    }]
  }
  t.throws(() => validate(given))
})

test('Check valid internationalized domain name (IDN)', async (t) => {
  const given = {
    hosts: [
      { domain: 'example.சிங்கப்பூர்' },
      { domain: toASCII('💩.com') }
    ]
  }
  t.doesNotThrow(() => validate(given))
})

test('Check valid IPv4 hostname', async (t) => {
  const given = { hosts: [{ domain: '123.45.67.89' }] }
  t.doesNotThrow(() => validate(given))
})

test('Check valid IPv6 hostname', async (t) => {
  const given = { hosts: [{ domain: '::1' }] }
  t.doesNotThrow(() => validate(given))
})

test('Valid host headers', async (t) => {
  const given = {
    hosts: [{
      headers: [{
        uri: '/search{?q,lang}',
        fields: {
          foo: 'bar',
          bla: ['one', 'two', 'three']
        }
      }]
    }]
  }
  t.doesNotThrow(() => validate(given))
})

test('Invalid host headers: missing fields', async (t) => {
  const given = {
    hosts: [{
      headers: [{
        uri: '/search{?q,lang}'
      }]
    }]
  }
  t.throws(() => validate(given), /should have required property 'fields'/)
})

test('Valid host headers: optional uri predicate', async (t) => {
  const given = {
    hosts: [{
      headers: [{
        fields: {
          foo: 'bar',
          bla: ['one', 'two', 'three']
        }
      }]
    }]
  }
  t.doesNotThrow(() => validate(given))
})

test('Invalid host headers: field name may be mixed case', async (t) => {
  const given = {
    hosts: [{
      headers: [{
        uri: '/',
        fields: {
          'mixed-case': 'yep',
          'Mixed-Case': 'nope'
        }
      }]
    }]
  }
  t.doesNotThrow(() => validate(given))
})

test('Throws an instance of Error', async (t) => {
  const given = { foo: 'bar' }
  t.plan(1)
  try {
    validate(given)
  } catch (error) {
    t.true(error instanceof Error)
  }
})

test('Invalid host headers: disallow connection headers', async (t) => {
  const given = {
    hosts: [{
      headers: [{
        uri: '/',
        fields: {
          'host': 'example.com'
        }
      }]
    }]
  }
  t.throws(() => validate(given), /property name 'host' is invalid/)
})

test('SNI processing', async (t) => {
  const given = {
    sni: {
      fallbackDomain: 'default.example.com',
      wildcards: [
        {
          suffix: '.example.com',
          domain: '*.example.com'
        }
      ]
    }
  }
  t.doesNotThrow(() => validate(given))
})

test('TLS certificates from file lookups', async (t) => {
  const given = {
    tls: {
      loader: 'file',
      fallbackCa: [
        '/etc/tls/ca.intermediary.pem',
        '/etc/tls/ca.root.pem'
      ],
      fallbackCert: '/etc/tls/cert.pem',
      fallbackKey: '/etc/tls/key.pem',
      storePath: '/var/tls/certs',
      keyPath: '$store/$domain/key.pem',
      certPath: '$store/$domain/cert.pem'
    }
  }
  t.doesNotThrow(() => validate(given))
})

test('TLS certificates from API calls', async (t) => {
  const given = {
    tls: {
      loader: 'core'
    }
  }
  t.doesNotThrow(() => validate(given))
})
