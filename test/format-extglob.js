const test = require('blue-tape')
const { extglobFormat } = require('../formats/extglobFormat')

test('relative path', async (t) => {
  t.true(extglobFormat('foo/bar'))
})

test('wildcard', async (t) => {
  t.true(extglobFormat('foo/**/*'))
})

test('wrong format', async (t) => {
  t.false(extglobFormat(123))
})
