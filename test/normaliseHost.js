const test = require('blue-tape')
const { normaliseHost } = require('..')
const { join } = require('path')

test('Sanity check', async (t) => {
  const actual = normaliseHost()
  t.equal(Promise.resolve(actual), actual)
  t.ok(await actual)
})

test('Host fallback', async (t) => {
  const actual = await normaliseHost({
    root: '/bla',
    fallback: '/foo/bar'
  })
  t.deepEqual(actual.fallback['200'], '/foo/bar')
  t.deepEqual(actual.fallback[200], '/foo/bar')
})

test('External manifest is normalised to an Array', async (t) => {
  const given = { manifest: 'fixtures/manifest/manifest.json' }
  const options = {
    externalManifest: true,
    configurationFilepath: join(__dirname, 'config.json')
  }
  const actual = await normaliseHost(given, options)
  const expected = [{
    get: '/index.html',
    push: [{ url: '/app.js' }]
  }]
  t.deepEqual(actual.manifest, expected)
})

test('External manifest is ignored', async (t) => {
  const given = { manifest: 'fixtures/manifest/manifest.json' }
  const options = {
    externalManifest: false,
    configurationFilepath: join(__dirname, 'config.json')
  }
  const actual = await normaliseHost(given, options)
  const expected = []
  t.deepEqual(actual.manifest, expected)
})

test('External manifest is disabled by default', async (t) => {
  const given = { manifest: 'fixtures/manifest/manifest.json' }
  const options = {
    externalManifest: undefined,
    configurationFilepath: join(__dirname, 'config.json')
  }
  const actual = await normaliseHost(given, options)
  const expected = []
  t.deepEqual(actual.manifest, expected)
})

test('External manifest must be resolved to a configuration', async (t) => {
  const given = { manifest: 'fixtures/manifest/manifest.json' }
  const options = {
    externalManifest: true,
    configurationFilepath: undefined
  }
  const actual = await normaliseHost(given, options)
  const expected = []
  t.deepEqual(actual.manifest, expected)
})

test('Header fields can be strings or arrays of strings', async (t) => {
  const actual = await normaliseHost({
    headers: [
      {
        uri: '{/path*}',
        fields: {
          'some-thing': 'hello world',
          'foo': ['bar', 'lol']
        }
      }
    ]
  })
  t.deepEqual(actual.headers, [
    {
      uri: '{/path*}',
      fields: {
        'some-thing': 'hello world',
        'foo': ['bar', 'lol']
      }
    }
  ])
})

test('Redirects require from', async (t) => {
  const given = [{ to: '/to' }]
  const actual = await normaliseHost({ redirects: given })
  const expected = []
  t.deepEqual(actual.redirects, expected)
})

test('Redirects require to', async (t) => {
  const given = [{ from: '/from' }]
  const actual = await normaliseHost({ redirects: given })
  const expected = []
  t.deepEqual(actual.redirects, expected)
})

test('Redirects default to status 308', async (t) => {
  const given = [{ from: '/from', to: '/to' }]
  const actual = await normaliseHost({ redirects: given })
  const expected = [{ from: '/from', to: '/to', status: 308 }]
  t.deepEqual(actual.redirects, expected)
})

test('Redirects can have a custom status code', async (t) => {
  const given = [{ from: '/from', to: '/to', status: 302 }]
  const actual = await normaliseHost({ redirects: given })
  const expected = [{ from: '/from', to: '/to', status: 302 }]
  t.deepEqual(actual.redirects, expected)
})

test('URL normalisation must be Boolean and defaults to true', async (t) => {
  t.deepEqual(
    (await normaliseHost({ directories: { resolve: true } }))
      .directories.resolve,
    true
  )
  t.deepEqual(
    (await normaliseHost({ directories: { resolve: false } }))
      .directories.resolve,
    false
  )
  t.deepEqual(
    (await normaliseHost({ directories: { resolve: 'not a boolean' } }))
      .directories.resolve,
    true
  )
  t.deepEqual(
    (await normaliseHost({ directories: {} }))
      .directories.resolve,
    true
  )
})
