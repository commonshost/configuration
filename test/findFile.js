const { join } = require('path')
const test = require('blue-tape')
const { findFile } = require('..')

test('Find default configuration file', async (t) => {
  const given = join(__dirname, 'fixtures/loader/found')
  const expected = join(given, 'commonshost.conf.js')
  const actual = await findFile(given)
  t.deepEqual(actual, expected)
})

test('Undefined if no file found', async (t) => {
  const given = join(__dirname, 'fixtures/loader/not-found')
  const expected = undefined
  const actual = await findFile(given)
  t.deepEqual(actual, expected)
})
