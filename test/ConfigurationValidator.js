const test = require('blue-tape')
const { ConfigurationValidator, validate } = require('..')

test('Backwards compatibility', async (t) => {
  const validator = new ConfigurationValidator()
  t.is(validator.validate, validate)
})
