const test = require('blue-tape')
const { calculationFormat } = require('../formats/calculationFormat')

test('allowed variable', async (t) => {
  t.true(calculationFormat('max_physical_cpu_cores'))
})

test('valid calculation', async (t) => {
  t.true(calculationFormat('max_physical_cpu_cores / 2'))
})

test('invalid expression', async (t) => {
  t.false(calculationFormat('console.log(123)'))
})

test('plain number is not a calculation', async (t) => {
  t.false(calculationFormat(4))
})
