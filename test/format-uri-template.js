const test = require('blue-tape')
const { uriTemplateFormat } = require('../formats/uriTemplateFormat')
const Ajv = require('ajv')

const given = '/redirect/{?url,}'

// If this ever succeeds, it might mean that
// Ajv has improved its uri-template format validator.
test('rejected by default regex accepted by custom validator', async (t) => {
  const ajv = new Ajv({ format: 'full' })
  ajv.addSchema({ type: 'string', format: 'uri-template' }, 'uri')
  t.false(ajv.validate('uri', given))
})

test('accepted by custom validator', async (t) => {
  t.true(uriTemplateFormat(given))
})
