const { join } = require('path')
const test = require('blue-tape')
const { loadFile } = require('..')

test('Execute async function from module', async (t) => {
  const given = join(__dirname, 'fixtures/loader/found/commonshost.conf.js')
  const expected = { foo: 'bar' }
  const actual = await loadFile(given)
  t.deepEqual(actual, expected)
})
