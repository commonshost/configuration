const test = require('blue-tape')
const { domainToASCII: toASCII } = require('url')
const { idnHostnameFormat } = require('../formats/idnHostnameFormat')

test('valid simple hostname', async (t) => {
  t.true(idnHostnameFormat('example.net'))
})

// https://www.verisign.com/en_US/channel-resources/domain-registry-products/idn/idn-policy/registration-rules/index.xhtml
// https://unicode.org/reports/tr46/
test('valid IDN hostname', async (t) => {
  for (const given of [
    'faß.de',
    'βόλος.com',
    'ශ්‍රී.com',
    'نامه‌ای.com',
    '스타벅스코리아.com'
  ]) {
    t.true(idnHostnameFormat(given))
  }
})

// https://tools.ietf.org/html/rfc5892
test('invalid IDN hostname', async (t) => {
  const given = '💩.example.net'
  t.false(idnHostnameFormat(given))
})

test('valid punycode hostname', async (t) => {
  const given = `${toASCII('💩')}.example.net`
  t.true(idnHostnameFormat(given))
})

test('hostname must be a string', async (t) => {
  for (const given of [123, null, true, {}, [], undefined]) {
    t.false(idnHostnameFormat(given))
  }
})

test('hostname label empty', async (t) => {
  const given = '..example.net'
  t.false(idnHostnameFormat(given))
})

test('hostname label length exceeds 63', async (t) => {
  const given = 'w'.repeat(64) + '.example.net'
  t.false(idnHostnameFormat(given))
})

test('hostname length exceeds 253', async (t) => {
  const given = '.example.net'.repeat(100).substring(1)
  t.false(idnHostnameFormat(given))
})
