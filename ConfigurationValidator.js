const Ajv = require('ajv')
const { schema: manifestSchema } = require('@commonshost/manifest')
const { hostSchema } = require('./schema/host')
const { serverSchema } = require('./schema/server')
const { extglobFormat } = require('./formats/extglobFormat')
const { calculationFormat } = require('./formats/calculationFormat')
const { idnHostnameFormat } = require('./formats/idnHostnameFormat')
const { uriTemplateFormat } = require('./formats/uriTemplateFormat')
const { normaliseHost } = require('./normaliseHost')
const { findFile } = require('./loader/findFile')
const { loadFile } = require('./loader/loadFile')

const ajv = new Ajv({ format: 'full' })
ajv.addFormat('calculation', calculationFormat)
ajv.addFormat('extglob', extglobFormat)
ajv.addFormat('idn-hostname', idnHostnameFormat)
ajv.addFormat('uri-template', uriTemplateFormat)
ajv.addSchema(manifestSchema)
ajv.addSchema(hostSchema)
ajv.addSchema(serverSchema)

class ConfigurationValidator {
  constructor () {
    this.validate = validate
  }
}

function validate (json) {
  const validity = ajv.validate('/Server', json)
  if (validity === true) {
    return true
  } else {
    const last = ajv.errors.pop()
    const message = `${last.dataPath} ${last.message} ${last.schemaPath}`
    throw new Error(message)
  }
}

module.exports.ConfigurationValidator = ConfigurationValidator
module.exports.validate = validate
module.exports.normaliseHost = normaliseHost
module.exports.findFile = findFile
module.exports.loadFile = loadFile
