module.exports.serverSchema = {
  $id: '/Server',
  type: 'object',
  definitions: {
    port: {
      type: 'integer',
      minimum: 1,
      maximum: 2 ** 16 - 1
    }
  },
  additionalProperties: false,
  properties: {
    signature: { type: 'boolean' },
    placeholder: {
      type: 'object',
      additionalProperties: false,
      properties: {
        hostNotFound: { type: 'string' }
      }
    },
    acme: {
      type: 'object',
      additionalProperties: false,
      properties: {
        proxy: {
          anyOf: [
            { type: 'string', format: 'uri' },
            { type: 'string', value: '' }
          ]
        },
        redirect: {
          anyOf: [
            { type: 'string', format: 'uri' },
            { type: 'string', value: '' }
          ]
        },
        webroot: { type: 'string' }
      }
    },
    core: {
      type: 'object',
      additionalProperties: false,
      properties: {
        origin: { type: 'string' },
        auth0: {
          type: 'object',
          additionalProperties: false,
          properties: {
            accessToken: { type: 'string' },
            issuer: { type: 'string' },
            audience: { type: 'string' },
            clientId: { type: 'string' },
            clientSecret: { type: 'string' }
          }
        }
      }
    },
    doh: {
      anyOf: [
        { type: 'boolean' },
        {
          type: 'object',
          additionalProperties: false,
          properties: {
            protocol: {
              type: 'string',
              enum: ['udp4', 'udp6']
            },
            localAddress: { type: 'string' },
            resolverAddress: { type: 'string' },
            resolverPort: { '$ref': '#/definitions/port' },
            timeout: { type: 'integer', minimum: 0 }
          }
        }
      ]
    },
    goh: {
      anyOf: [
        { type: 'boolean' },
        {
          type: 'object',
          additionalProperties: false,
          properties: {
            allowHTTP1: { type: 'boolean' },
            unsafeAllowNonStandardPort: { type: 'boolean' },
            unsafeAllowPrivateAddress: { type: 'boolean' },
            timeout: { type: 'integer', minimum: 0 }
          }
        }
      ]
    },
    gopher: {
      anyOf: [
        { type: 'boolean' },
        {
          type: 'object',
          additionalProperties: false,
          properties: {
            plaintextFallback: { type: 'string' },
            port: { '$ref': '#/definitions/port' },
            secureRoot: { type: 'string' }
          }
        }
      ]
    },
    log: {
      type: 'object',
      additionalProperties: false,
      properties: {
        level: {
          type: 'string',
          enum: ['silent', 'fatal', 'error', 'warn', 'info', 'debug', 'trace']
        }
      }
    },
    http: {
      type: 'object',
      additionalProperties: false,
      properties: {
        redirect: { type: 'boolean' },
        from: { '$ref': '#/definitions/port' },
        to: { '$ref': '#/definitions/port' }
      }
    },
    http2: {
      type: 'object',
      additionalProperties: false,
      properties: {
        timeout: {
          anyOf: [
            { type: 'number', minimum: 0 },
            { type: 'null' }
          ]
        }
      }
    },
    https: {
      type: 'object',
      additionalProperties: false,
      properties: {
        port: { '$ref': '#/definitions/port' }
      }
    },
    hosts: {
      type: 'array',
      items: { '$ref': '/Host' }
    },
    push: {
      type: 'object',
      additionalProperties: false,
      properties: {
        diaryTotalItems: { type: 'integer', minimum: 0 },
        diaryBitsPerItem: { type: 'integer', minimum: 1, maximum: 64 }
      }
    },
    sni: {
      type: 'object',
      additionalProperties: false,
      properties: {
        fallbackDomain: {
          oneOf: [
            { type: 'string', format: 'idn-hostname' },
            { type: 'string', const: '' }
          ]
        },
        wildcards: {
          type: 'array',
          items: {
            type: 'object',
            additionalProperties: false,
            properties: {
              suffix: { type: 'string' },
              domain: { type: 'string' }
            }
          }
        }
      }
    },
    tls: {
      required: ['loader'],
      oneOf: [
        {
          additionalProperties: false,
          properties: {
            loader: { type: 'string', const: 'file' },
            fallbackCa: {
              type: 'array',
              uniqueItems: true,
              items: { type: 'string' }
            },
            fallbackCert: { type: 'string' },
            fallbackKey: { type: 'string' },
            storePath: { type: 'string' },
            keyPath: { type: 'string' },
            certPath: { type: 'string' }
          }
        },
        {
          additionalProperties: false,
          properties: {
            loader: { type: 'string', const: 'core' }
          }
        }
      ]
    },
    via: {
      pseudonym: { type: 'string' }
    },
    workers: {
      type: 'object',
      additionalProperties: false,
      properties: {
        count: {
          anyOf: [
            { type: 'integer', minimum: 1 },
            { type: 'string', format: 'calculation' }
          ]
        }
      }
    },
    www: {
      redirect: { type: 'boolean' }
    }
  }
}
