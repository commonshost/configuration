module.exports.hostSchema = {
  $id: '/Host',
  type: 'object',
  additionalProperties: false,
  properties: {
    domain: {
      anyOf: [
        { type: 'string', format: 'idn-hostname' },
        { type: 'string', format: 'ipv4' },
        { type: 'string', format: 'ipv6' }
      ]
    },
    root: { type: 'string' },
    fallback: {
      oneOf: [
        { type: 'string' },
        {
          type: 'object',
          additionalProperties: false,
          patternProperties: {
            '[245]\\d\\d': { type: 'string' }
          }
        }
      ]
    },
    directories: {
      type: 'object',
      additionalProperties: false,
      properties: {
        resolve: { type: 'boolean' },
        trailingSlash: {
          type: 'string',
          enum: ['always', 'never']
        }
      }
    },
    accessControl: {
      type: 'object',
      additionalProperties: false,
      properties: {
        allowOrigin: {
          anyOf: [
            { type: 'string', enum: ['*'] },
            { type: 'string', format: 'uri' }
          ]
        }
      }
    },
    headers: {
      type: 'array',
      items: {
        type: 'object',
        additionalProperties: false,
        required: ['fields'],
        properties: {
          uri: { type: 'string', format: 'uri-template' },
          fields: {
            type: 'object',
            additionalProperties: false,
            patternProperties: {
              '^[A-Za-z0-9-]+$': {
                anyOf: [
                  { type: 'string' },
                  {
                    type: 'array',
                    items: { type: 'string' }
                  }
                ]
              }
            },
            propertyNames: {
              not: {
                enum: [
                  'connection',
                  'upgrade',
                  'host',
                  'http2-settings',
                  'keep-alive',
                  'proxy-connection',
                  'transfer-encoding'
                ]
              }
            }
          }
        }
      }
    },
    redirects: {
      type: 'array',
      items: {
        type: 'object',
        additionalProperties: false,
        required: ['from', 'to'],
        properties: {
          from: { type: 'string', format: 'uri-template' },
          to: { type: 'string', format: 'uri-template' },
          status: { type: 'integer', minimum: 300, maximum: 399 }
        }
      }
    },
    manifest: {
      anyOf: [
        { '$ref': '/Manifest' },
        { type: 'string' }
      ]
    }
  }
}
