const { resolve, dirname } = require('path')
const { domainToASCII: toASCII } = require('url')
const { normalise } = require('@commonshost/manifest')
const { loadFile } = require('./loader/loadFile')

function defaultHost () {
  return {
    domain: '',
    root: '',
    fallback: {},
    directories: {
      resolve: true,
      trailingSlash: 'always'
    },
    accessControl: {
      allowOrigin: '*'
    },
    headers: [],
    redirects: [],
    manifest: []
  }
}

module.exports.normaliseHost = async function normaliseHost (
  host = {},
  {
    configurationFilepath,
    externalManifest = false
  } = {}
) {
  const normalised = defaultHost()

  if (typeof host.domain === 'string') {
    normalised.domain = toASCII(host.domain).toLowerCase()
  }

  if (typeof host.root === 'string') {
    normalised.root = resolve(host.root)
  }

  if (typeof host.fallback === 'string') {
    normalised.fallback['200'] = host.fallback
  }
  if (typeof host.fallback === 'object') {
    for (const status of Object.keys(host.fallback)) {
      if (!isNaN(status) && status >= 200 && status < 600) {
        if (typeof host.fallback[status] === 'string') {
          normalised.fallback[status] = host.fallback[status]
        }
      }
    }
  }

  if (typeof host.directories === 'object') {
    const allowed = ['always', 'never']
    if (allowed.includes(host.directories)) {
      normalised.directories.trailingSlash = host.directories
    }
    if (typeof host.directories.resolve === 'boolean') {
      normalised.directories.resolve = host.directories.resolve
    }
  }

  if (typeof host.accessControl === 'object' &&
    typeof host.accessControl.allowOrigin === 'string'
  ) {
    normalised.accessControl.allowOrigin =
      host.accessControl.allowOrigin
  }

  if (Array.isArray(host.headers)) {
    for (const { uri, fields } of host.headers) {
      const header = { fields: {} }
      if (typeof uri === 'string') {
        header.uri = uri
      }
      for (const [ name, value ] of Object.entries(fields)) {
        if (typeof value === 'string') {
          header.fields[name] = value
        } else if (Array.isArray(value)) {
          header.fields[name] = Array.from(value)
        }
      }
      normalised.headers.push(header)
    }
  }

  if (Array.isArray(host.redirects)) {
    for (const { from, to, status } of host.redirects) {
      if (typeof from === 'string' && typeof to === 'string') {
        const redirect = { from, to }
        if (Number.isInteger(status) && status >= 300 && status <= 399) {
          redirect.status = status
        } else {
          redirect.status = 308
        }
        normalised.redirects.push(redirect)
      }
    }
  }

  if (Array.isArray(host.manifest)) {
    normalised.manifest = normalise(host.manifest)
  }
  if (
    typeof host.manifest === 'string' &&
    externalManifest === true &&
    typeof configurationFilepath === 'string'
  ) {
    const base = dirname(configurationFilepath)
    const filepath = resolve(base, host.manifest)
    const manifest = await loadFile(filepath)
    normalised.manifest = normalise(manifest)
  }

  return normalised
}
